---
# You don't need to edit this file, it's empty on purpose.
# Edit theme's home layout instead if you wanna make some changes
# See: https://jekyllrb.com/docs/themes/#overriding-theme-defaults
layout: single
permalink: /
---

# Welcome!

Welcome to the Democratic Socialists of America National Tech Committee Request
For Comment system! Here you can find a list of projects that the national tech
committee has proposed, the rationale and reasoning behind those projects. In
addition you can propose projects yourself as well that you would like the
National Tech Committee to work on. We're still figuring out how to make all
this work and it's all very exciting. So, please, join in and help us build
democratic socialism!
