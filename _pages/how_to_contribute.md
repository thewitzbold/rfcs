---
layout: single
title: How To Contribute
permalink: /how-to-contribute/
---

To contribute in any way to the RFC system, we ask you to please follow [our code of conduct](someday). 

If you have any questions hop into dsausa.chat and ask over at #tech-committee and we can help you out.  

N.B. figured out how to cut like three steps out of this, gonna cut this way down.

To create a new RFC, please do the following: 
* [Sign into Gitlab](https://gitlab.com/users/sign_in). Gitlab is a website that
   hosts the code for this website and allows us to use version control to keep
   track of changes to that code. You can create an account on Gitlab for free
   and you can also sign in with Google or Twitter. Additionally, you may use an
   account with a fake name when creating the account. We do not require that
   you identify yourself up front when submitting an RFC, but we may ask you to
   identify yourself in private to a member of the tech committee later on.
* [Fork the rfcs project](https://gitlab.com/dsausa/rfcs/forks/new). Forking a
  project on gitlab means that you create a copy of the project for yourself
  that you have permission to change.
* Create a new branch on your fork of the rfcs project. If your username was
  `KarlMarx` then you would go
  to
  [https://gitlab.com/KarlMarx/rcfs/branches/new](https://gitlab.com/KarlMarx/rcfs/branches/new) to
  create the fork. Give the fork a descriptive name related to your project
  idea. For example, if you were proposing a new page on the website dedicated
  to pictures of socialist cats, naming it something like
  `create-socialist-cat-page` would fit well.
* Create a new file corresponding to the new RFC. If your username was KarlMarx
  and you had created a branch named `create-socialist-cat-page`, then to create
  a new file you would go to [https://gitlab.com/KarlMarx/rfcs/new/twitter-account-proposal/_rfcs](https://gitlab.com/KarlMarx/rfcs/new/twitter-account-proposal/_rfcs).
  Name the file `new.md`. Copy the template
  from [here](https://gitlab.com/ZackMaril/rfcs/raw/master/_rfcs/0.md) and paste
  it into the text box. You don't have to make any more changes, just click
  Commit Changes at the bottom of the page.
* Create a merge request. A merge request will notify members of the tech
  committee that you have started working on a new proposal. To create one,
  click merge request at the top of the page, it's a bright blue button in the
  upper right hand corner. If you cannot find it, go to the equivalent
  of
  [https://gitlab.com/KarlMarx/rfcs/blob/create-socialist-cat-page/_rfcs/new.md](https://gitlab.com/KarlMarx/rfcs/blob/create-socialist-cat-page/_rfcs/new.md) and
  look for the button. Add `WIP:` to the title to indicate that it is a work in
  progress. Maybe write a description, up to you. Then submit the merge request.
* Once you've submitted the merge request, you'll be taken to the `dsausa/rfcs`
  page corresponding to the merge request. It may take a minute, but `Zack!`
  will eventually comment with a link to preview how the proposed rfc looks on
  the website. As you work and change the rfc in your own fork, the website will
  be updating to reflect those changes.
* Now that the paper work is out of the way, you can start filling out the RFC
  for real. Go back to your fork and start editing `new.md` and filling out all
  the sections in the template. Commit your changes to your branch and they will
  automatically show up in the merge requests.
* As you get further along writing things up, folks may start engaging with your
  post more and making suggestions about things!


